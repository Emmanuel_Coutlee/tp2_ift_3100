\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {french}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Document de design}{1}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}Sommaire}{1}{subsection.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2}Interactivit\IeC {\'e}}{1}{subsection.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3}Technologie}{1}{subsection.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.4}Architecture}{1}{subsection.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.5}Fonctionnalit\IeC {\'e}s}{1}{subsection.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.6}Ressources}{1}{subsection.1.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.7}Pr\IeC {\'e}sentation}{1}{subsection.1.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Image}{2}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Importation}{2}{subsection.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Exportation}{2}{subsection.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}Espace de couleur}{2}{subsection.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4}Traitement d'image}{2}{subsection.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5}Image proc\IeC {\'e}durale}{2}{subsection.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Dessin vectoriel}{3}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Curseur dynamique}{3}{subsection.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Primitives vectorielles}{3}{subsection.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}Forme vectorielles}{3}{subsection.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4}Outils de dessin}{3}{subsection.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5}Interface}{3}{subsection.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Transformation}{4}{section.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Transformation interactive}{4}{subsection.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}Structure de sc\IeC {\`e}ne}{4}{subsection.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3}S\IeC {\'e}lection multiple}{4}{subsection.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4}Coordonn\IeC {\'e}es non-cart\IeC {\'e}siennes}{4}{subsection.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5}Historique}{4}{subsection.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}G\IeC {\'e}om\IeC {\'e}trie}{5}{section.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1}Particules}{5}{subsection.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2}Primitives}{5}{subsection.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3}Mod\IeC {\`e}le}{5}{subsection.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4}Texture}{5}{subsection.5.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.5}G\IeC {\'e}om\IeC {\'e}trie proc\IeC {\'e}durale}{5}{subsection.5.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Cam\IeC {\'e}ra}{6}{section.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Propri\IeC {\'e}t\IeC {\'e}s de cam\IeC {\'e}ra}{6}{subsection.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2}Mode de projection}{6}{subsection.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3}Cam\IeC {\'e}ra interactive}{6}{subsection.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.4}Cam\IeC {\'e}ra multiple}{6}{subsection.6.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.5}Cam\IeC {\'e}ra anim\IeC {\'e}e}{6}{subsection.6.5}
