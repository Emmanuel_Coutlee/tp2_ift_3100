#include "GlobalGui.h"



GlobalGui::GlobalGui()
{
	globalMode = nullptr;
}


GlobalGui::~GlobalGui()
{
}

void GlobalGui::setup(Mode& m)
{
	ofLog() << "I was here";
	globalMode = &m;
	mode.addListener(this, &GlobalGui::toggleMode);
	GeneralControls.setup();
	GeneralControls.setPosition(ofGetWidth() / 2, 0);
	//GeneralControls.add(text.setup("GlobalSettings", "GlobalSettings"));
	GeneralControls.add(mode.setup("GraphicsMode", 100,100));
	//GeneralControls.add(slider.setup("test", 1, -1.0, 10));
	//GeneralControls.add(textMode.setup(*EnumToString(*globalMode),""));
	

}

void GlobalGui::draw()
{

	GeneralControls.draw();
}

void GlobalGui::update( Mode** mode)
{
	if(globalMode != *mode)
	{
		mode = &globalMode;
	}
}

void GlobalGui::toggleMode()
{
	Mode t;
	ofLog() << "I was here";
	switch(*globalMode)
	{
	case threeD: t = vectors;/* textMode.setName(*EnumToString(t))*/; globalMode = &t; break;
	case vectors: t = images; /*textMode.setName(*EnumToString(t))*/; globalMode = &t; break;
	case images:t = threeD; /*textMode.setName(*EnumToString(t))*/; globalMode = &t; break;
	//default: ;
	}
	
}
std::string GlobalGui::EnumToString(Mode v)
{
	string a;
	
	switch (v)
	{
	case vectors: a = "vectors";  return a; break;
	case threeD:  a = "threeD"; return a; break;
	case images:a = "images"; return a; break;
	default:  a = "[Unknown Mode_type]";    return a; break;
	}
}
