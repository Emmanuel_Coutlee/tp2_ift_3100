 
#include "camera.h"



Camera::Camera(){

	lastScreenX = 0;
	lastScreenY = 0;

	cameraFov = 60.0f;
	cameraNear = 50.0f;
	cameraFar = 1750.0f;

	cam = new ofCamera();

	cameraPosition = { 0,0,-1000 };
	cameraTarget = ofVec3f(0.0f, 0.0f, 0.0f);

	cam->setupPerspective(false, cameraFov, cameraNear, cameraFar, ofVec2f(0, 0));

	cam->setPosition(cameraPosition);
	cam->lookAt(cameraTarget);
}

void Camera::setClip(float farclip, float nearclip) {
	cameraNear = nearclip;
	cameraFar = farclip;

	cam->setNearClip(cameraNear);
	cam->setFarClip(cameraFar);

}

void Camera::setFov(float fov) {
	cam->setFov(fov);
}
ofCamera* Camera::get(){
	return cam;
}

void Camera::translation(int x, int y, int z) {
	cam->truck(lastScreenX - x);
	cam->boom(y - lastScreenY);
	
	lastScreenX = x;
	lastScreenY = y;
}

void Camera::setlastScreenPos(int x,int  y) {

	lastScreenX = x;
	lastScreenY = y;

}

void Camera::tourner(int x, int y) {

	ofQuaternion xRot(ofDegToRad((lastScreenX-x)*5), cam->getYAxis());
	ofQuaternion yRot(ofDegToRad((lastScreenY-y)*5), cam->getXAxis());

	ofQuaternion quatRotation = yRot * xRot;

	if (cam->getPosition().z <= cameraTarget.z +10  && cam->getPosition().z >= cameraTarget.z -10 && cam->getPosition().x <= cameraTarget.x + 10 && cam->getPosition().x >= cameraTarget.x -10) {
		if (cam->getPosition().y > 0 && lastScreenY - y <= 0)
			quatRotation = xRot;
		if (cam->getPosition().y < 0 && lastScreenY - y >= 0)
			quatRotation = xRot;
	}
	lastScreenX = x;
	lastScreenY = y;
	
	cam->rotateAround(quatRotation, cameraTarget);
	cam->lookAt(cameraTarget);

}

void Camera::avancer(float scroll) {
		cam->dolly(-10 * scroll);
}

void Camera::setPerspective(bool perspective){

	if (perspective) {
		cam->disableOrtho();
	}
	else {
		cam->enableOrtho();
	}

}

bool Camera::isPerspective() {
	return !cam->getOrtho();
}


void Camera::setCameraTarget(ofVec3f v) {
	cameraTarget = v;
}
RayCast Camera::castRay(ofVec2f point) const
{

	float h = tan(cam->getFov());
	float w = h * cam->getAspectRatio();
	ofVec3f direction = cameraForward + point.x * w * cam->getSideDir() + point.y * h * cam->getUpDir();



	return RayCast(cameraPosition, direction.getNormalized());
}
void Camera::rayTrace(ObjectSet* objectsInScene)
{
	for (int x = 0; x < ofGetWidth(); x++)
	{
		for (int y = 0; y < ofGetHeight(); y++)
		{
			ofVec2f screenCoord((2.0f*x) / ofGetWidth() - 1.0f,
				(-2.0f*y) / ofGetHeight() + 1.0f);
			RayCast ray = this->castRay(screenCoord);

			//ofColor* curPixel = objectsInScene.colo;

			Intersection intersection(ray);
			if (objectsInScene->IsIntersectRelevant(intersection))
			{
				objectsInScene->setColorCall(intersection.color, intersection.position());
			}
			/*else
			{
			*curPixel = Color(0.0f);
			}*/
		}
	}
}

Camera::~Camera(){
	if (nullptr != cam)
		delete cam;
}