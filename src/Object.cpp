#include "Object.h"

Object::Object()
{
	parent = nullptr;
	index = 0;
	objName = "Root";

	objColor = (0, 0, 0); 
}

Object::~Object()
{
	if(childList.size()> 0)
	{
		for (std::list<Object*>::iterator it = childList.begin(); it != childList.end(); ++it)
		{
			delete *it;
			*it = nullptr;
		}
	}

	parent = nullptr;
}

void Object::draw()
{
	for (std::list<Object*>::const_iterator it = childList.begin(); it != childList.end(); ++it)
	{
		(*it)->draw();
	}
}

void Object::deleteThis()
{
	if (parent != nullptr)
	{
		parent->deleteChild(index);
	}
}

void Object::addChild(Object* obj)
{
	childList.push_back(obj);
	obj->setIndex(childList.size() - 1);
	obj->setParentObj(this);
}

void Object::deleteChild(int index)
{
	std::list<Object*>::const_iterator it = childList.begin();
	advance(it, index);
	delete *it;
	childList.erase(it);
}

void Object::setParentObj(Object* obj)
{
	parent = obj;
	setParent(*parent);
}

void Object::setIndex(int i)
{
	index = i;
}

std::string Object::getName() const
{
	return objName;
}

void Object::setName(std::string name)
{
	objName = name;
}

void Object::updateTransform()
{

}

void Object::setPositionCall(ofVec3f newPos)
{
}

void Object::setRotationCall(ofVec3f)
{
}

void Object::setScaleCall(ofVec3f)
{
}

void Object::setToRelativeTransform()
{
}


void Object::setColorCall(ofColor newCol)
{
	objColor = newCol;
}

Object* Object::getParentObj() const
{
	return parent;
}

const ofVec3f Object::getPositionCall() const
{
	return ofVec3f(0, 0, 0);
}

const ofVec3f Object::getRotationCall() const
{
	return ofVec3f(0, 0, 0);
}

const ofVec3f Object::getScaleCall() const
{
	return ofVec3f(1, 1, 1);
}

const ofVec3f Object::getRelativePositionCall() const
{
	return ofVec3f(0, 0, 0);
}

const ofVec3f Object::getRelativeRotationCall() const
{
	return ofVec3f(0, 0, 0);
}

const ofVec3f Object::getRelativeScaleCall() const
{
	return ofVec3f(1, 1, 1);
}

const ofColor Object::getColorCall() const
{
	return objColor;
}


void Object::setSelect(bool b) {
	if (b && !isSelecting) {
		isSelecting = true;
		temp = objColor;
		selectColor = ofColor((int)ofRandom(0,255), (int)ofRandom(0,255), (int)ofRandom(0,255) );
		objColor = selectColor;
	}
	if (!b) {
		objColor = temp;
		isSelecting = false;
		//ofLog() << selectColor;
	}
	for (auto i = childList.begin(); i != childList.end(); ++i) {
		(*i)->setSelect(b);
	}
}


Object * Object::findSelection(ofColor c) {
	if (selectColor == c) {
		return this;
	}
	else {
		for (auto i = childList.begin(); i != childList.end(); ++i) {
			Object* te = (*i)->findSelection(c);
			if (te != nullptr) {
				return te;

			}
		}
	}
	return nullptr;
}

/*bool Object::IsIntersectRelevant(Intersection& intersection)
{
	return false;
}

bool Object::doesObjectIntersect(const RayCast& ray)
{
	return false;
}*/
const ofVec3f Object::getmilieux() const{
	return ofVec3f(0.0f, 0.0f, 0.0f);
}