﻿#include "CourbeParam.h"

CourbeParam::CourbeParam()
{
	courbeMode = bezier;
	pointChoisi = 0;
	pointX = { 0 };
	pointY = { 0 };
}
CourbeParam::~CourbeParam()
{
	
}

void CourbeParam::Draw()
{
	switch (courbeMode)
	{
	case bezier:
		courbe.addVertex(ofPoint((pointX[0], pointY[0])));
		courbe.bezierTo(pointX[1], pointY[1], pointX[2], pointY[2], pointX[3], pointY[3]);
		courbe.draw();
		break;
	case catmull4:
		for(int i = 0; i <= 4; i++)
		{
			courbe.curveTo(ofPoint(pointX[i], pointY[i]));
		}
		courbe.draw();
		break;
	case catmull5:
		for (int i = 0; i <= 5; i++)
		{
			courbe.curveTo(ofPoint(pointX[i], pointY[i]));
		}
		courbe.draw();
		break;
	}
}

int CourbeParam::getPointChoisi()
{
	return pointChoisi;
}

void CourbeParam::setPointChoisi(int p)
{
	pointChoisi = p;
}

void CourbeParam::modifiX(int x)
{
	pointX[pointChoisi] = x;
}

void CourbeParam::modifiY(int y)
{
	pointX[pointChoisi] = y;
}
