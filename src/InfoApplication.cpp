
// IFT3100H17_Camera/application.cpp
// Classe principale de l'application.


#include "Cube.h"
#include "Sphere.h"
#include "Plane.h"
#include "InfoApplication.h"
#include "Tetrahedron.h"

#include "Object3D.h"


InfoApplication::InfoApplication()
{
	renderer = nullptr;
	camera = nullptr;
	gui3D = nullptr;
	listSelect = nullptr;

	gGui = nullptr;

	vectGraphics = nullptr;
	mouseIsDragged = false;
	ctrlKeyIsDown = false;
}

void InfoApplication::setup()
{
	
	ofLog() << "<application::setup>";
	cursorDrawImage.load("paintBrush.jpg");
	cursorDeleteImage.load("eraser.png");
	cursorColorImage.load("image");
	renderer = new Renderer();
	camera = new Camera();
	gui3D = new Gui3D();
	sceneObjectsRay = new ObjectSet();
	listSelect = new ListeSelection();

	gGui = new GlobalGui();
	gui3D->setCamera(camera);
	
	


	gui3D->setSelectObj(renderer->root);
	gui3D->setRenderer(renderer);
	gui3D->TransformCreate();
	
	gGui->setup(currentmode);
	renderer->setCamera(camera->get());


	renderer->setup();
	
}

void InfoApplication::update()
{
	currentmode = gui3D->paramEditMode.get();
	drawingMode = gui3D->GetShape();
	if (currentmode == GlobalGui::vectors)
	{
		if (limit>1 && limit<MAX_POINTS && vectGraphics != nullptr)
		{
			if(drawingMode== VectorGraphics::Point)
			{
				vectGraphics->drawPoint(points, limit, 100);
			}
			if(drawingMode==VectorGraphics::Line)
			{
				vectGraphics->drawLine(points, limit);
			}
			
			
		}
		if(vectGraphics != nullptr)
		{
			vectGraphics->update();
		
		}

	}
	if(currentmode== GlobalGui::threeD)
	{
		if(IMAFIRINMAHLAZOR)
		{
			camera->rayTrace(sceneObjectsRay);
		}
		
		renderer->update();
	}
	gui3D->update();
	
	
}

void InfoApplication::draw()
{
	
	//gGui->draw();
	if (currentmode == GlobalGui::threeD)
	{
		ofSetWindowShape(ofGetWindowHeight() * gui3D->getLastRatio(), ofGetHeight());
		renderer->draw();
		
	}
	if(currentmode == GlobalGui::vectors)
	{
		if(vectGraphics != nullptr)
		{

			vectGraphics->draw();
		}
		if(ctrlKeyIsDown)
		{
			ofHideCursor();
			cursorDrawImage.draw(mouseX, mouseY);
		}
		if(drawEraser)
		{
			cursorDeleteImage.draw(mouseX, mouseY);
		}

		
	}
	if(currentmode == GlobalGui::images)
	{
		
	}
	ofShowCursor();
	gui3D->draw();
	
	
}

void InfoApplication::keyPressed(int key) {

	//TODO 
	switch (key)
	{
	case 't':
		{
			if(IMAFIRINMAHLAZOR)
			{
				IMAFIRINMAHLAZOR = false;
			}
			else
			{
				IMAFIRINMAHLAZOR = true;
			}
		}
			case 'q': {//control
				ctrlKeyIsDown = true;
				drawPaintBrush = true;
			}
					   break;
			case 'e':
				{
				drawEraser = true;
			}break;

	}
}

void InfoApplication::keyReleased(int key)
{
	//TODO 
	switch (key)
	{
		case 'q': {//control
			ctrlKeyIsDown = false;
			drawPaintBrush = false;
		}
		case 'e':
		{
			drawEraser = false;
		};
			break;
		case 'z':
		{
			
			renderer->selection = renderer->selection->getParentObj();
			gui3D->setSelectObj(renderer->selection);
			gui3D->TransformCreate();
		}
		break;

		case 'd':
		{
			Object* tetrahedron = new Tetrahedron;
			tetrahedron->setParentObj(renderer->selection);
			renderer->selection->addChild(tetrahedron);

			renderer->selection = tetrahedron;
			gui3D->setSelectObj(renderer->selection);
			gui3D->TransformCreate();
			listSelect->clear();

		}
		break;

		case 'c':
		{
			if (renderer->selection->selectColor == listSelect->selectColor)
				renderer->selection = renderer->root;
			Object* cube = new Cube();
			cube->setParentObj(renderer->selection);
			renderer->selection->addChild(cube);
			
			renderer->selection = cube;
			gui3D->setSelectObj(renderer->selection);
			gui3D->TransformCreate();
			listSelect->clear();
		}
		break;

		case 'p':
		{
		}
		break;

		case 's':
		{
			if (renderer->selection->selectColor == listSelect->selectColor)
				renderer->selection = renderer->root;
			Object* sphere = new Sphere();
			sphere->setParentObj(renderer->selection);
			renderer->selection->addChild(sphere);
			sceneObjectsRay->addShape(sphere);

			renderer->selection = sphere;
			gui3D->setSelectObj(renderer->selection);
			gui3D->TransformCreate();
			listSelect->clear();
		}
		break;

		case 'a':
		{
			if (renderer->selection->selectColor == listSelect->selectColor)
				renderer->selection = renderer->root;
			Object* plane = new Plane();
			plane->setParentObj(renderer->selection);
			renderer->selection->addChild(plane);

			sceneObjectsRay->addShape(plane);

			renderer->selection = plane;
			gui3D->setSelectObj(renderer->selection);
			gui3D->TransformCreate();
			listSelect->clear();
		}
		break;

		case ' ':
			renderer->imageExport("screenshot", "png");
			break;
	}

}

void InfoApplication::mouseReleased(int x, int y, int button) {
	limit = 0;
	if (currentmode == GlobalGui::threeD)
	{
		if ((!mouseIsDragged) && button == 0) {
			renderer->select(x, y);
			if (ctrlKeyIsDown && renderer->selection->getName() != renderer->root->getName()) {
				listSelect->addObj(renderer->selection);
				gui3D->setSelectObj(listSelect);
				gui3D->TransformSelect();
			}
			else {
				gui3D->setSelectObj(renderer->selection);
				gui3D->TransformSelect();

				listSelect->clear();
			}
		}
		mouseIsDragged = false;
	}
	

}
	

void InfoApplication::mouseDragged(int x, int y, int button) {
	
	if (currentmode == GlobalGui::threeD)
	{
		if (mouseIsDragged) {
			if (button == 0) {
				camera->translation(x, y, 0);
			}
			else if (button == 2) {
				camera->tourner(x, y);
			}
		}
		else {
			camera->setlastScreenPos(x, y);
			mouseIsDragged = true;
		}
	}
	if (currentmode == GlobalGui::vectors)
	{
		ofLog() << vectGraphics;
		if(vectGraphics != NULL && drawingMode == VectorGraphics::Point || drawingMode == VectorGraphics::Line)
		{
			if (abs(points[limit].x - x) > 1 && abs(points[limit].y - y) >1)
			{
				if (limit<MAX_POINTS)
				{
					points[limit].x = x;
					points[limit].y = y;
					limit++;

				}
				
			}
		
		}
		
		
		//limit = 0;

	}
}

void InfoApplication::mouseScrolled(int x, int y, float scrollX, float scrollY) {
	camera->avancer(scrollY);
}

void InfoApplication::mousePressed(int x, int y, int button)
{
	limit = 0;
	if(currentmode == GlobalGui::vectors)
	{
		limit = 0;
		if (button == OF_MOUSE_BUTTON_1 && ctrlKeyIsDown)
		{
			if(drawingMode != VectorGraphics::Point || drawingMode != VectorGraphics::Line)
			{
				if(vectGraphics == nullptr )
				{
					vectGraphics = new VectorGraphics(x, y, drawingMode);
					gui3D->AddParameterGroup(vectGraphics->generalVectorModifiers);
					
				}
				else
				{
					vectGraphics->AddShape(x, y, vectGraphics->scale, drawingMode);
				}
				vectGraphics->updatePolygonArray();
				gui3D->AddParameterGroup(vectGraphics->polygons[vectGraphics->drawnShapes.size()-1]->polygonParams);
				
			}
			
			
			

		}
		if (button == OF_MOUSE_BUTTON_1 && drawEraser)
		{
			/*for (list<VectorPolygon*>::iterator ti = vectGraphics->drawnShapes.begin(); ti != vectGraphics->drawnShapes.end(); ti++)
			{

				VectorPolygon* temp = *ti;
				if(temp->posX.get()-mouseX < temp->scale && temp->posY.get() - mouseY < temp->scale)
				{
					delete temp;
				}

			}*/
		}
	}

}


void InfoApplication::exit()
{
	ofLog() << "<application::exit>";
}

InfoApplication::~InfoApplication()
{
	if (nullptr != renderer)
		delete renderer;
	if (nullptr != camera)
		delete camera;
	if (nullptr != vectGraphics)
		delete vectGraphics;
	if (nullptr != gui3D)
		delete gui3D;
	if (nullptr != listSelect)
		delete listSelect;
}
