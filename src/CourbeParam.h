﻿#pragma once
#include <array>
#include "ofPolyline.h"

enum courbeType{bezier,catmull4,catmull5};

class CourbeParam
{
private:
	std::array<int, 5> pointX;
	std::array<int, 5> pointY;
	int pointChoisi;
	courbeType courbeMode;
	ofPolyline courbe;


public:
	CourbeParam();
	~CourbeParam();

	void Draw();
	void setPointChoisi(int p);
	int getPointChoisi();
	void modifiX(int x);
	void modifiY(int y);
};
