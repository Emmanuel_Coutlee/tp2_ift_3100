#include "VectorPolygon.h"
#include "VectorGraphics.h"

void VectorPolygon::setup(int x, int y)
{
	polygonParams.add(numberOfPolygons.set("Shape", VectorGraphics::Triangle,1,60));

	polygonParams.add(posX.set("Position X", x, 0, ofGetWidth()));
	polygonParams.add(posY.set("Position Y", y, 0, ofGetHeight()));

	polygonParams.add(scale.set("Scale",100,10, ofGetWidth()));
	polygonParams.add(rotateZ.set("Rotation Z", 0.0, -1.0, 1.0));

	polygonParams.add(red.set("R", 255, 0, 255));
	polygonParams.add(green.set("G", 255, 0, 255));
	polygonParams.add(blue.set("B", 255, 0, 255));
	polygonParams.add(alpha.set("A", 255, 0, 255));




}

void VectorPolygon::update()
{
	rotationZ = rotationZ + rotateZ;
}

void VectorPolygon::draw(ofFbo* f)
{
	f->begin();
	ofPushStyle();
	if(numberOfPolygons>0)
	{
		ofSetCircleResolution(numberOfPolygons + 2);
		//ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2);
		ofSetColor(red, green, blue, alpha);
		ofDrawCircle(posX, posY, scale);
	}
	else
	{
		for (int i = 0; i < limit - 1; i++)
		{
			ofDrawLine(points[i].x, points[i].y, points[i + 1].x, points[i + 1].y);
		}
	}
	
	
	
	ofPopStyle();
	f->end();

}

void VectorPolygon::toPath()
{
	 
}

void VectorPolygon::SetPoints(ofPoint p[])
{
	for(int i =0; i< p->length(); i++)
	{
		points[i] = p[i];
	}
}

VectorPolygon::VectorPolygon(ofPoint p[],int l, int thick)
{
	SetPoints(p);
	thickness = thick;
	limit = l;
}

VectorPolygon::VectorPolygon(int x, int y)
{
	setup(x, y);
}
VectorPolygon::VectorPolygon(int x, int y,int dimension, int numPoly)
{
	setup(x, y);
	numberOfPolygons.set(numPoly);
	scale.set(dimension);
}


VectorPolygon::~VectorPolygon()
{
}
