﻿#include "Plane.h"
#include "ofCamera.h"

Plane::Plane()
{
	plane = new ofPlanePrimitive();
	plane->setHeight(100);
	plane->setWidth(100);
	plane->setPosition(0, 0, 0);
	plane->setScale(1, 1, 1);
	//plane->set(1, 1);
	
	objName = "Plane";

	objColor.set(0, 0, 0);
}

Plane::Plane(float px, float py, float pz, float h, float l)
{
	plane = new ofPlanePrimitive();
	plane->setPosition(px, py, pz);
	plane->setHeight(h);
	plane->setWidth(l);
}

Plane::~Plane()
{

	delete plane;
}

void Plane::draw()
{
	ofSetColor(objColor);
	plane->draw();

	for (std::list<Object*>::const_iterator it = childList.begin(); it != childList.end(); ++it)
	{
		(*it)->draw();
	}
}

void Plane::setPositionCall(ofVec3f newPos)
{
	ofVec3f parentPos = parent->getPositionCall();
	plane->setPosition(newPos + parentPos);
}

void Plane::setRotationCall(ofVec3f newRot)
{
	ofVec3f parentRot = parent->getRotationCall();
	plane->setOrientation(newRot + parentRot);
}

void Plane::setScaleCall(ofVec3f newScale)
{
	ofVec3f parentScale = parent->getScaleCall();
	plane->setScale(newScale * parentScale);
}

const ofVec3f Plane::getPositionCall() const
{
	return plane->getPosition();
}

const ofVec3f Plane::getRotationCall() const
{
	return plane->getOrientationEuler();
}

const ofVec3f Plane::getScaleCall() const
{
	return plane->getScale();
}

const ofVec3f Plane::getRelativePositionCall() const
{
	return plane->getPosition() - parent->getPositionCall();
}

const ofVec3f Plane::getRelativeRotationCall() const
{
	return plane->getOrientationEuler() - parent->getRotationCall();
}

const ofVec3f Plane::getRelativeScaleCall() const
{
	return plane->getScale() / parent->getScaleCall();
}

const ofVec3f Plane::getmilieux() const {
	return getPositionCall();
}
const void Plane::setNormal(ofPoint& point)
{
	this->getMesh().setNormal(1, point);
}

const ofVec3f Plane::getNormal()
{
	return this->getMesh().getNormal(1);
}

bool Plane::IsIntersectRelevant(Intersection& intersection)
{
	
	float dDotN = intersection.ray.direction.dot(normal);

	if (dDotN == 0.0f)
	{
		
		return false;
	}

	
	ofPoint temp = this->getPositionCall() - intersection.ray.origin;
	float t = temp.dot(normal) / dDotN;

	if (t <= RAY_T_MIN || t >= intersection.t)
	{
		
		return false;
	}

	intersection.t = t;
	//intersection.pObject = this->getParentObj();
	intersection.color = this->getColorCall();

	return true;

}

bool Plane::doesObjectIntersect(const RayCast& ray)
{


	//On regard s'il y a intersection
	float isIntersect = ray.direction.dot(normal);

	if (isIntersect == 0.0f)
	{
		
		return false;
	}

	// Trouver le point d'intersection
	ofPoint temp = this->getPositionCall() - ray.origin;
	float distance = temp.dot(normal) / isIntersect;

	if (distance <= RAY_T_MIN || distance >= ray.tMax)
	{
		
		return false;
	}

	return true;
}
