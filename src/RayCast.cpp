#include "RayCast.h"

RayCast::RayCast()
	: origin(0.0f, 0.0f, 0.0f),
	direction(),
	tMax(RAY_T_MAX)
{
}

RayCast::RayCast(const RayCast& r)
	: origin(r.origin),
	direction(r.direction),
	tMax(r.tMax)
{
}

RayCast::RayCast(const ofPoint& origin, const ofVec3f& direction, float tMax)
	: origin(origin),
	direction(direction),
	tMax(tMax)
{
}

RayCast::~RayCast()
{
}

RayCast& RayCast::operator =(const RayCast& r)
{
	origin = r.origin;
	direction = r.direction;
	tMax = r.tMax;
	return *this;
}

ofPoint RayCast::calculate(float t) const
{
	float X = origin.x + direction.x;
	float Y = origin.y + direction.y;
	float Z = origin.z + direction.z;
	X *= t;
	Y *= t;
	Z *= t;
	return ofPoint(X,Y,Z);
}


Intersection::Intersection()
	: ray(),
	t(RAY_T_MAX)
//,pObject(NULL)
{
}

Intersection::Intersection(const Intersection& i)
	: ray(i.ray),
	t(i.t)
//,pObject(i.pObject)
{
}

Intersection::Intersection(const RayCast& ray)
	: ray(ray),
	t(ray.tMax)
//,pObject(NULL)
{
}

Intersection::~Intersection()
{
}

Intersection& Intersection::operator =(const Intersection& i)
{
	ray = i.ray;
	t = i.t;
	//pObject = i.pObject;
	return *this;
}

bool Intersection::IsRayIntersectRelevant() const
{
	return false;// (pObject != NULL);
}

ofPoint Intersection::position() const
{
	return ray.calculate(t);
}
