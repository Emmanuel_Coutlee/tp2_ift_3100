#pragma once
#include "ofPoint.h"
#include "ofColor.h"
#include "Object.h"


// In order to prevent bouncing rays self-intersecting
#define RAY_T_MIN 0.0001f

// 'Infinite' distance, used as a default value
#define RAY_T_MAX 1.0e30f

class RayCast
{
public:
	ofPoint origin; // start
	ofVec3f direction;
	float tMax;

	RayCast();
	RayCast(const RayCast& r);
	RayCast(const ofPoint& origin, const ofVec3f& direction,
		float tMax = RAY_T_MAX);

	virtual ~RayCast();

	RayCast& operator =(const RayCast& r);

	ofPoint calculate(float t) const;
};


struct Intersection
{
	RayCast ray;
	float t;
//	Object* pObject;
	ofColor color;

	Intersection();
	Intersection(const Intersection& i);
	Intersection(const RayCast& ray);

	virtual ~Intersection();

	Intersection& operator =(const Intersection& i);

	bool IsRayIntersectRelevant() const;
	ofPoint position() const;
};

