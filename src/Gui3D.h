#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "camera.h"
#include "Object.h"
#include "renderer.h"
#include "GlobalGui.h"
#include "VectorGraphics.h"

class Gui3D
{
public:
	Gui3D();
	~Gui3D();
	void setup();
	void update();
	void draw();

	float calculateFov();

	void setTranformToDraw();
	void setColorToDraw();

	void setCamera(Camera * c);
	void setSelectObj(Object* obj);
	void setRenderer(Renderer* ren);
	void TransformCreate();
	void TransformSelect();
	void updateGui(ofVec3f pos, ofVec3f rot, ofVec3f scale,ofColor colorRBG);

	float getLastRatio() const;
	ofParameter<GlobalGui::Mode> paramEditMode;
	VectorGraphics::Shape GetShape();
	void AddParameterGroup(ofParameterGroup group);


private:
	void toggleEditMode();
	
	float lastHorizontale;
	float lastVerticale;
	float lastRatio;

	bool selectionIsDeleted;

	Camera * cam;
	Object* objSelection;
	Renderer* render;
	GlobalGui* gGui;
	std::list<Object*> listObjSelection;



	//gui camera
	ofxPanel camera;

	ofxFloatSlider clippingAvant;
	ofxFloatSlider clippingArriere;
	ofxFloatSlider visionHorizontale;
	ofxFloatSlider visionVerticale;
	ofxFloatSlider ratioAspect;
	ofxToggle percepective;
	ofxLabel textCamera;
	ofxToggle teinteBool;
	ofParameter<ofColor> teinteCamera;

	//gui transform
	ofxPanel guiTransform;
	ofxPanel guiColor;

	ofxLabel textTransform;
	ofxButton renameButton;
	ofxButton deleteButton;
	ofParameter<ofVec3f> paramPosition;
	ofParameter<ofVec3f> paramRotation;
	ofParameter<ofVec3f> paramScale;
	ofxButton positionReset;
	ofxButton rotationReset;
	ofxButton scaleReset;
	ofParameter<bool> colorEnable;
	
	ofxLabel textPanneauColor;
	ofParameter<bool> hsbSlider;
	ofParameter<ofColor> paramColorRGB;
	ofParameter<ofVec3f> paramColorHSB;
	ofxButton colorHexInput;
	ofxLabel textColorHex;
	ofParameter<int> paramColorAlpha;

	//gui vectors
	ofxPanel guiVectors;
	ofParameterGroup guiVectorGroup;
	ofParameter<int> paramShapeMode;
	ofParameter<std::string> currentShapeMode;
	//gui general
	ofxPanel guiGeneral;


	ofxToggle stateButton;
	ofxButton button3d;
	ofxButton buttonVect;
	ofxButton buttonImage;
	ofParameter<std::string> currentEditMode;


};
