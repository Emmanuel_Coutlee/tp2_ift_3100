﻿#include "Tetrahedron.h"

Tetrahedron::Tetrahedron()
{
	tetrahedon = new of3dPrimitive;
	tetrahedon->getMesh().addVertex(ofVec3f(0, 1, 0)*3);
	tetrahedon->getMesh().addVertex(ofVec3f(sqrt(3)/2, 0, 0)*3);
	tetrahedon->getMesh().addVertex(ofVec3f(-sqrt(1/2),0, -1/2)*3);
	tetrahedon->getMesh().addVertex(ofVec3f(sqrt(1/2), 0, 1/2)*3);
	
	//tetrahedon->getMesh().addTriangle();
	tetrahedon->getMesh().setMode(OF_PRIMITIVE_TRIANGLE_FAN);

	tetrahedon->setPosition(0, 0, 0);
	tetrahedon->setScale(1, 1, 1);
	objName = "tetrahedron";
}

Tetrahedron::~Tetrahedron()
{
	delete tetrahedon;
}

void Tetrahedron::draw()
{
	ofSetColor(objColor);
	tetrahedon->draw();

	if (childList.size() > 0)
	{
		for (std::list<Object*>::const_iterator it = childList.begin(); it != childList.end(); ++it)
		{
			(*it)->draw();
		}
	}
}

void Tetrahedron::updateTransform()
{
}

void Tetrahedron::setPositionCall(ofVec3f newPos)
{
	ofVec3f parentPos = parent->getPositionCall();
	tetrahedon->setPosition(newPos + parentPos);
}

void Tetrahedron::setRotationCall(ofVec3f newRot)
{
	ofVec3f parentRot = parent->getRotationCall();
	tetrahedon->setOrientation(newRot + parentRot);
}

void Tetrahedron::setScaleCall(ofVec3f newScale)
{
	ofVec3f parentScale = parent->getScaleCall();
	tetrahedon->setScale(newScale * parentScale);
}

void Tetrahedron::setToRelativeTransform()
{
}

const ofVec3f Tetrahedron::getPositionCall() const
{
	return tetrahedon->getPosition();
}

const ofVec3f Tetrahedron::getRotationCall() const
{
	return  tetrahedon->getOrientationEuler();
}

const ofVec3f Tetrahedron::getScaleCall() const
{
	return  tetrahedon->getScale();
}

const ofVec3f Tetrahedron::getmilieux() const
{
	return getPositionCall();
}

const ofVec3f Tetrahedron::getRelativePositionCall() const
{
	return tetrahedon->getPosition() - parent->getPositionCall();
}

const ofVec3f Tetrahedron::getRelativeRotationCall() const
{
	return tetrahedon->getOrientationEuler() - parent->getRotationCall();

}

const ofVec3f Tetrahedron::getRelativeScaleCall() const
{
	return tetrahedon->getScale() / parent->getScaleCall();
}
