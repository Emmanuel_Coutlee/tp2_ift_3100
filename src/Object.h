#pragma once
#include <list>
#include <string>
#include "ofVec3f.h"
#include "ofColor.h"
#include "of3dPrimitives.h"
#include "ofMain.h"
#include "RayCast.h"

class Object: public of3dPrimitive
{
public:
	Object();
	virtual ~Object();

	//virtual void setup();
	virtual void draw();
	//virtual void update();
	virtual void deleteThis();
	virtual void addChild(Object* obj);
	virtual void deleteChild(int index);
	virtual void setParentObj(Object* obj);
	virtual void setIndex(int i);

	virtual std::string getName() const;
	virtual void setName(std::string name);

	virtual void updateTransform();

	virtual void setPositionCall(ofVec3f);
	virtual void setRotationCall(ofVec3f);
	virtual void setScaleCall(ofVec3f);

	virtual void setToRelativeTransform();
	
	virtual Object* getParentObj() const;

	virtual const ofVec3f getPositionCall() const;
	virtual const ofVec3f getRotationCall() const;
	virtual const ofVec3f getScaleCall() const;

	virtual const ofVec3f getmilieux() const;

	virtual const ofVec3f getRelativePositionCall() const;
	virtual const ofVec3f getRelativeRotationCall() const;
	virtual const ofVec3f getRelativeScaleCall() const;

	
	
	virtual void setColorCall(ofColor);
	virtual const ofColor getColorCall() const;

	void setSelect(bool b);

	Object * findSelection(ofColor c);

	/*virtual bool IsIntersectRelevant(Intersection& intersection);
	virtual bool doesObjectIntersect(const RayCast& ray);*/

	ofColor selectColor;
protected:
	Object* parent;
	int index;
	std::list<Object*> childList;
	std::string objName;
	ofColor objColor;

	bool isSelecting = false;
	ofColor temp;
};

