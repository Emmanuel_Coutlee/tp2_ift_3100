#pragma once
#include "ofxGui.h"
#include "ofAppNoWindow.h"

class GlobalGui
{

public:
	enum Mode { threeD, vectors, images };
	GlobalGui();
	~GlobalGui();
	void setup(Mode& mode);
	void draw();
	void update( Mode** mode);

	
	
	ofxButton mode;
	//ofxLabel text;
	//ofxLabel textMode;
	ofxFloatSlider slider;

	ofxPanel GeneralControls;

	Mode* globalMode;

	void toggleMode();
	string EnumToString(Mode v);
	

};

