﻿#include "Cube.h"
#include "ofCamera.h"

Cube::Cube()
{
	cube = new ofBoxPrimitive(100,100,100);
	cube->setPosition(0, 0, 0);
	cube->setScale(1, 1, 1);

	objName = "cube";
}

Cube::Cube(float px, float py, float pz, float rx, float ry, float rz)
{
	cube = new ofBoxPrimitive(px, py, pz);
	//cube->rotate()
}

Cube::~Cube()
{
	delete cube;
}

void Cube::draw()
{
	ofSetColor(objColor);
	cube->draw();

	if (childList.size() > 0)
	{
		for (std::list<Object*>::const_iterator it = childList.begin(); it != childList.end(); ++it)
		{
			(*it)->draw();
		}
	}
}

void Cube::updateTransform()
{
	setPositionCall(getPositionCall());
	setRotationCall(getRotationCall());
	setScaleCall(getScaleCall());

	for (std::list<Object*>::iterator it = childList.begin(); it != childList.end(); ++it)
	{
		(*it)->updateTransform();
	}
}


void Cube::setPositionCall(ofVec3f newPos)
{

	ofVec3f parentPos = parent->getPositionCall();
	cube->setPosition(newPos + parentPos);
	
}

void Cube::setRotationCall(ofVec3f newRot)
{

	ofVec3f parentRot = parent->getRotationCall();
	cube->setOrientation(newRot +parentRot);

}

void Cube::setScaleCall(ofVec3f newScale)
{

	ofVec3f parentScale = parent->getScaleCall();
	cube->setScale(newScale * parentScale);

}

void Cube::setToRelativeTransform()
{
	cube->setPosition(getRelativeRotationCall());
	cube->setOrientation(getRelativeRotationCall());
	cube->setScale(getRelativeScaleCall());

	for (std::list<Object*>::iterator it = childList.begin(); it != childList.end(); ++it)
	{
		(*it)->setToRelativeTransform();
	}
}

const ofVec3f Cube::getPositionCall() const
{
	return cube->getPosition();
}

const ofVec3f Cube::getRotationCall() const
{
	return cube->getOrientationEuler();
}

const ofVec3f Cube::getScaleCall() const
{
	return cube->getScale();
}

const ofVec3f Cube::getRelativePositionCall() const
{
	return cube->getPosition() - parent->getPositionCall();
}

const ofVec3f Cube::getRelativeRotationCall() const
{
	return cube->getOrientationEuler() - parent->getRotationCall();
}

const ofVec3f Cube::getRelativeScaleCall() const
{
	return cube->getScale() / parent->getScaleCall();
}
const ofVec3f Cube::getmilieux() const {
	return getPositionCall();
}
