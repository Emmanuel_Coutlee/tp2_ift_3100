﻿#pragma once
#include "Object.h"


class Tetrahedron:public Object
{
public:

	Tetrahedron();
	~Tetrahedron();

	void draw() override;

	void updateTransform() override;

	void setPositionCall(ofVec3f) override;
	void setRotationCall(ofVec3f) override;
	void setScaleCall(ofVec3f) override;

	void setToRelativeTransform() override;

	const ofVec3f getPositionCall() const override;
	const ofVec3f getRotationCall() const override;
	const ofVec3f getScaleCall() const override;

	const ofVec3f getmilieux() const override;

	const ofVec3f getRelativePositionCall() const override;
	const ofVec3f getRelativeRotationCall() const override;
	const ofVec3f getRelativeScaleCall() const override;

private:
	of3dPrimitive* tetrahedon;
	
};
