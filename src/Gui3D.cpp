
#include "Gui3D.h"
#include "VectorGraphics.h"
#include <MacTypes.h>

Gui3D::Gui3D() {
	cam = nullptr;
	objSelection = nullptr;
	render = nullptr;
	gGui = nullptr;

	selectionIsDeleted = false;

	setup();
}

Gui3D::~Gui3D()
{
}

void Gui3D::setup() {
	
	gGui = new GlobalGui();
	lastHorizontale = 60.0f;
	lastVerticale = ofGetWindowHeight() * lastHorizontale / ofGetWindowWidth();
	lastRatio = lastHorizontale / lastVerticale;

	camera.setup(); // most of the time you don't need a name
	camera.add(textCamera.setup("Camera", ""));
	camera.add(percepective.setup("perspective", true));
	camera.add(clippingAvant.setup("clipping avant", 50.0f, 10.0f, 1000.0f));
	camera.add(clippingArriere.setup("clipping arriere", 1750.0f, 1000.0f, 2500.0f));
	camera.add(visionHorizontale.setup("vision horizontale", lastHorizontale, 10.0f, 180.0f));
	camera.add(visionVerticale.setup("vision verticale", lastVerticale, 10.0f, 180.0f));
	camera.add(ratioAspect.setup("ratio d'aspect", lastRatio, 0.1f, 3.0f));
	camera.add(teinteBool.setup("teinte", false));
	camera.add(teinteCamera.set(ofColor(128, 0, 128, 70)));

	guiTransform.setup();
	guiTransform.setPosition(ofGetWindowWidth() - 225, 10);

	guiColor.setup();
	guiColor.setPosition(ofGetWindowWidth() - 225, 380);
	//gui General----------------------------------------------------
	guiGeneral.setup();
	guiGeneral.setPosition(ofGetWidth() / 2, 10);

	guiGeneral.add(stateButton.setup("ToggleEditMode"));
	guiGeneral.add(button3d.setup("3D"));
	guiGeneral.add(buttonVect.setup("vect"));
	guiGeneral.add(buttonImage.setup("Image"));
	paramEditMode.set(GlobalGui::vectors);
	std::string value;
	if (gGui->EnumToString(paramEditMode.get()) == "threeD")
	{
		value = "3D";
	}
	else
	{
		value = gGui->EnumToString(paramEditMode.get());
	}
	guiGeneral.add(currentEditMode.set("EditMode", value));
	//gui Vectors
	
	guiVectorGroup.add(paramShapeMode.set("Shape Mode",VectorGraphics::Line, VectorGraphics::Line, VectorGraphics::Point));
	VectorGraphics temp = VectorGraphics();
	guiVectorGroup.add(currentShapeMode.set(temp.EnumToString(paramShapeMode)));
	guiVectors.setup(guiVectorGroup);
	guiVectors.setPosition(ofGetWidth()*0.05, ofGetHeight()*0.1);
}
void Gui3D::update()
{

	if(paramEditMode.get()== GlobalGui::vectors)
	{
		VectorGraphics temp = VectorGraphics();
		currentShapeMode.set(temp.EnumToString(paramShapeMode.get()));
	}

	std::string value;
	if (button3d && gGui->EnumToString(paramEditMode.get()) != "threeD") {
		paramEditMode.set(GlobalGui::threeD);
		value = "3D";
		currentEditMode.set("EditMode", value);
	}
	else if (buttonImage && gGui->EnumToString(paramEditMode.get()) != "images") {
		paramEditMode.set(GlobalGui::images);
		value = gGui->EnumToString(paramEditMode.get());
		currentEditMode.set("EditMode", value);
	}
	else if (buttonVect && gGui->EnumToString(paramEditMode.get()) == "vectors") {
		paramEditMode.set(GlobalGui::vectors);
		value = gGui->EnumToString(paramEditMode.get());
		currentEditMode.set("EditMode", value);
	}
	
	if (colorHexInput)
	{
		string hexStringValue = ofSystemTextBoxDialog("Entr�e la valeur hexad�cimal de la couleur voulu.", "0xFFFFFF");
		int hexIntValue = ofHexToInt(hexStringValue);
		ofColor newColor;
		newColor.setHex(hexIntValue);
		paramColorRGB.set(newColor);
		textColorHex.setup("Couleur : " + ofToHex(paramColorRGB->getHex()));
		objSelection->setColorCall(paramColorRGB);
		paramColorHSB.set(ofVec3f(paramColorRGB->getHue(), paramColorRGB->getSaturation(), paramColorRGB->getBrightness()));

	}

	if(renameButton)
	{
		string newName = ofSystemTextBoxDialog("Entr�e le nouveau nom de l'objet", "nouveauNom");
		objSelection->setName(newName);
		textTransform.setup("Objet", objSelection->getName());
	}
	
	if (deleteButton)
	{
		if (!selectionIsDeleted)
		{
			Object* obj = objSelection->getParentObj();
			if (obj != nullptr)
			{
				render->selection->deleteThis();
				render->selection = obj;
				setSelectObj(render->selection);
				selectionIsDeleted = true;
				TransformSelect();
			}
		}
	}
	else
	{
		selectionIsDeleted = false;
	}
	cam->setCameraTarget(objSelection->getmilieux());
	
}

void Gui3D::setCamera(Camera * c) {
	cam = c;
}

void Gui3D::setSelectObj(Object* obj)
{
	objSelection = obj;
}

void Gui3D::setRenderer(Renderer* ren)
{
	render = ren;
}

void Gui3D::TransformCreate()
{
	ofVec3f pos = objSelection->getPositionCall();
	ofVec3f rot = objSelection->getRotationCall();
	ofVec3f scale = objSelection->getScaleCall();

	ofColor colorRBG = objSelection->getColorCall();
	
	updateGui(pos, rot, scale, colorRBG);
}

void Gui3D::TransformSelect()
{
	ofVec3f pos = objSelection->getRelativePositionCall();
	ofVec3f rot = objSelection->getRelativeRotationCall();
	ofVec3f scale = objSelection->getRelativeScaleCall();

	ofColor colorRBG = objSelection->getColorCall();

	updateGui(pos, rot, scale, colorRBG);
	ofLog() << objSelection->getName() << "gui: " << rot;
}

void Gui3D::updateGui(ofVec3f pos, ofVec3f rot, ofVec3f scale, ofColor colorRBG)
{

	ofVec3f colorHSB = ofVec3f(colorRBG.getHue(), colorRBG.getSaturation(), colorRBG.getBrightness());
	int colorHex = colorRBG.getHex();

	guiTransform.clear();

	guiTransform.add(textTransform.setup("Objet", objSelection->getName()));
	guiTransform.add(renameButton.setup("Renommer l'objet"));
	guiTransform.add(deleteButton.setup("Supprimer l'objet"));
	guiTransform.add(paramPosition.set("Position", pos, ofVec3f(-1000.0f), ofVec3f(1000.0f)));
	guiTransform.add(positionReset.setup("Reset position"));
	guiTransform.add(paramRotation.set("Rotation", rot, ofVec3f(-360.0f), ofVec3f(360.0f)));
	guiTransform.add(rotationReset.setup("Reset rotation"));
	guiTransform.add(paramScale.set("Scale", scale, ofVec3f(0.0f), ofVec3f(50.0f)));

	guiTransform.add(scaleReset.setup("Reset scale"));
	guiTransform.add(colorEnable.set("Panneau Couleur", false));

	guiColor.clear();

	guiColor.add(textPanneauColor.setup("Panneau couleur", ""));
	guiColor.add(hsbSlider.set("Utiliser HSB", false));
	guiColor.add(colorHexInput.setup("Entr�e Hex couleur"));
	guiColor.add(textColorHex.setup("Couleur Hex", ofToHex(colorHex)));
	guiColor.add(paramColorRGB.set("Couleur RGBA", colorRBG, ofColor(0, 0), ofColor(255, 255)));

	guiColor.add(paramColorHSB.set("Couleur HSB", colorHSB, ofVec3f(0, 0, 0), ofVec3f(255, 255, 255)));
	guiColor.add(paramColorAlpha.set("a", colorRBG.a, 0, 255));
}

float Gui3D::getLastRatio() const
{
	return lastRatio;
}



float Gui3D::calculateFov() {
	
	if (lastHorizontale != visionHorizontale) {
		visionVerticale = visionHorizontale / ratioAspect;
	}
	else if(lastRatio == ratioAspect){
		visionHorizontale = visionVerticale * ratioAspect;
	}
	else {
		visionHorizontale = visionVerticale * ratioAspect;
	}
	
	//verification des max
	if (visionHorizontale > visionHorizontale.getMax() || visionHorizontale < visionHorizontale.getMin()) {
		visionHorizontale = lastHorizontale;
		visionVerticale = lastVerticale;
		ratioAspect = lastRatio;
	}
	if (visionVerticale > visionVerticale.getMax() || visionVerticale < visionVerticale.getMin()) {
		visionHorizontale = lastHorizontale;
		visionVerticale = lastVerticale;
		ratioAspect = lastRatio;
	}

	lastHorizontale = visionHorizontale;
	lastVerticale = visionVerticale;
	lastRatio = ratioAspect;

	return lastHorizontale;

}

void Gui3D::setTranformToDraw()
{
	if (!positionReset)
	{
		objSelection->setPositionCall(paramPosition);
	}
	else
	{
		objSelection->setPositionCall(ofVec3f(0, 0, 0));
		paramPosition.set(ofVec3f(0, 0, 0));
	}

	if (!rotationReset)
	{
		objSelection->setRotationCall(paramRotation);
	}
	else
	{
		objSelection->setRotationCall(ofVec3f(0, 0, 0));
		paramRotation.set(ofVec3f(0, 0, 0));
	}

	if (!scaleReset)
	{
		objSelection->setScaleCall(paramScale);
	}
	else
	{
		objSelection->setScaleCall(ofVec3f(1, 1, 1));
		paramScale.set(ofVec3f(1, 1, 1));
	}
}

void Gui3D::setColorToDraw()
{
	if (!hsbSlider)
	{
		objSelection->setColorCall(paramColorRGB);
		paramColorHSB.set(ofVec3f(paramColorRGB->getHue(), paramColorRGB->getSaturation(), paramColorRGB->getBrightness()));
		textColorHex.setup("Couleur : " + ofToHex(paramColorRGB->getHex()));
		paramColorAlpha.set(paramColorRGB->a);
	}
	else
	{
		ofColor newColor;
		newColor.setHsb(paramColorHSB->x, paramColorHSB->y, paramColorHSB->z);
		newColor.a = paramColorAlpha;
		objSelection->setColorCall(newColor);
		paramColorRGB.set(newColor);
		textColorHex.setup("Couleur : " + ofToHex(paramColorRGB->getHex()));
	}
}


void Gui3D::draw() {
	ofDisableLighting();
	ofDisableDepthTest();
	if(paramEditMode.get()==GlobalGui::threeD)
	{
		
		if (cam != nullptr) {
			cam->setPerspective(percepective);
			cam->setClip(clippingArriere, clippingAvant);
			cam->setFov(calculateFov());
			if (teinteBool)
				render->setTeinte(teinteCamera);
			else
				render->setTeinte(ofColor(0, 0, 0, 0));
		}

		if (objSelection != nullptr)
		{
			setTranformToDraw();
			if (colorEnable)
			{
				setColorToDraw();
			}
		}
		camera.draw();
		guiTransform.draw();
		
	}
	if (paramEditMode.get() == GlobalGui::vectors)
	{
		guiVectors.draw();
	}
	
	if (colorEnable)
	{
		guiColor.draw();
	}
	guiGeneral.draw();
}
//Gui general----------------------------------------------------------------------------
void Gui3D::toggleEditMode()
{
	switch (paramEditMode)
	{
	case GlobalGui::threeD: paramEditMode.set(GlobalGui::vectors); break;
	case GlobalGui::vectors: paramEditMode.set(GlobalGui::images); break;
	case GlobalGui::images:paramEditMode.set(GlobalGui::threeD); break;
	default: paramEditMode.set(GlobalGui::threeD);
	}
	std::string value;
	if (gGui->EnumToString(paramEditMode.get()) == "threeD")
	{
		value = "3D";
	}
	else
	{
		value = gGui->EnumToString(paramEditMode.get());
	}
	currentEditMode.set("EditMode", value);
}


VectorGraphics::Shape Gui3D::GetShape()
{
	switch(paramShapeMode.get())
	{
	case VectorGraphics::Triangle: return VectorGraphics::Triangle; break;
	case VectorGraphics::Rectangle:return VectorGraphics::Rectangle; break;
	case VectorGraphics::Pentagon:return VectorGraphics::Pentagon; break;
	case VectorGraphics::Hexagone:return VectorGraphics::Hexagone; break;
	case VectorGraphics::Line:return VectorGraphics::Line; break;
	case VectorGraphics::Point:return VectorGraphics::Point; break;
	default: return VectorGraphics::Triangle; break;
	}
}

void Gui3D::AddParameterGroup(ofParameterGroup group)
{
	//gui Vectors
	guiVectors.clear();
	guiVectorGroup.add(group);
	guiVectors.setup(guiVectorGroup);
}



