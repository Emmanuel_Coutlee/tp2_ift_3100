#pragma once

#include "Object.h"
#include "VectorPolygon.h"
#include "ofxPanel.h"


class VectorGraphics//: public Object
{
public:
	enum Shape { Line, Triangle, Rectangle,Pentagon,Hexagone,Point};
	float scale = 1;
	VectorGraphics();
	VectorGraphics(int px, int py,Shape shape);
	~VectorGraphics();
	void update();
	void draw();
	void updatePolygonArray();
	std::list<VectorPolygon*> drawnShapes =  list<VectorPolygon*>();
	std::vector<VectorPolygon*> polygons;
	void AddShape(int x, int y,int scale, int numPoly);
	void drawLine(ofPoint points[],int limit);
	void drawPoint(ofPoint points[], int limit, int radius);
	void setup();
	string EnumToString(int shape);
	ofParameterGroup generalVectorModifiers;
	ofxPanel guiVectorModifier;
private:
	bool listHasValue = false;
	bool deleteShapes = false;
	ofFbo* fbo = new ofFbo();
	
	ofParameter<int> hardness;
};

