
#include "ListeSelection.h"


ListeSelection::ListeSelection() {
	pos = ofVec3f(0.0f, 0.0f, 0.0f);
	rot = ofVec3f(0.0f, 0.0f, 0.0f);
	scale = ofVec3f(1.0f,1.0f,1.0f);
	objName = "";
}

ListeSelection::~ListeSelection() {

}


void ListeSelection::draw() {
	for (auto i = listObjet.begin(); i != listObjet.end(); ++i) {
		(*i)->draw();
	}
}

void ListeSelection::updateTransform() {

	for (auto i = listObjet.begin(); i != listObjet.end(); ++i) {
		(*i)->updateTransform();
	}
}

void ListeSelection::setPositionCall(ofVec3f v) {
	ofVec3f diff = ofVec3f(pos.x - v.x, pos.y - v.y, pos.z - v.z);
	pos = v;
	for (auto i = listObjet.begin(); i != listObjet.end(); ++i) {
		ofVec3f posObj = (*i)->getRelativePositionCall();
		(*i)->setPositionCall(ofVec3f(diff.x + posObj.x, diff.y + posObj.y, diff.z + posObj.z));
	}
}

void ListeSelection::setRotationCall(ofVec3f v) {
	ofVec3f diff = ofVec3f(rot.x - v.x, rot.y - v.y, rot.z - v.z);
	rot = v;
	for (auto i = listObjet.begin(); i != listObjet.end(); ++i) {
		ofVec3f rotObj = (*i)->getRelativeRotationCall();
		(*i)->setRotationCall(ofVec3f(diff.x + rotObj.x, diff.y + rotObj.y, diff.z + rotObj.z));
	}
}

void ListeSelection::setScaleCall(ofVec3f v) {
	ofVec3f diff = ofVec3f(v.x / scale.x, v.y / scale.y, v.z / scale.z);
	scale = v;
	for (auto i = listObjet.begin(); i != listObjet.end(); ++i) {
		ofVec3f scaleObj = (*i)->getRelativeScaleCall();
		(*i)->setScaleCall(ofVec3f(diff.x * scaleObj.x, diff.y * scaleObj.y, diff.z * scaleObj.z));
	}
}

const ofVec3f ListeSelection::getPositionCall() const {
	return pos;
}

const ofVec3f ListeSelection::getRotationCall() const {
	return rot;
	ofLog() << rot;
}

const ofVec3f ListeSelection::getScaleCall() const {
	return scale;
}

const ofVec3f ListeSelection::getRelativePositionCall() const {
	return getPositionCall();
}

const ofVec3f ListeSelection::getRelativeRotationCall() const {
	return getRotationCall();
}

const ofVec3f ListeSelection::getRelativeScaleCall() const {
	return getScaleCall();
}

void ListeSelection::addObj(Object * o) {
	for (auto i = listObjet.begin(); i != listObjet.end(); ++i) {
		if ((*i)->selectColor == o->selectColor) {
			return;
		}
	}
	objName = objName +","+ o->getName();
	listObjet.push_back(o);

	pos = ofVec3f(0.0f, 0.0f, 0.0f);
	rot = ofVec3f(0.0f, 0.0f, 0.0f);
	scale = ofVec3f(1.0f, 1.0f, 1.0f);

}

void ListeSelection::clear() {
	pos = ofVec3f(0.0f, 0.0f, 0.0f);
	rot = ofVec3f(0.0f, 0.0f, 0.0f);
	scale = ofVec3f(1.0f, 1.0f, 1.0f);
	objName = "";
	listObjet.clear();
}

const ofVec3f ListeSelection::getmilieux() const {
	ofVec3f milieu = ofVec3f(0.0f, 0.0f, 0.0f);
	for (auto i = listObjet.begin(); i != listObjet.end(); ++i) {
			ofVec3f posObj = (*i)->getPositionCall();
			milieu = ofVec3f(milieu.x + posObj.x, milieu.y + posObj.y, milieu.z + posObj.z);
	}
	int n = listObjet.size();
	return ofVec3f(milieu.x / n, milieu.y / n, milieu.z / n);
}


 void ListeSelection::setColorCall(ofColor c) {
	 objColor = c;
	 for (auto i = listObjet.begin(); i != listObjet.end(); ++i) {
		 (*i)->setColorCall(c);
	 }
}