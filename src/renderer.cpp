#include "renderer.h"

Renderer::Renderer()
{
	root = new Object();
	selection = root;

	light = new ofLight();
}



void Renderer::setup() 
{
	
	ofEnableDepthTest();
	ofEnableLighting();

	// activer la lumi�re
	light->setPosition(0, 0, -1000);
	light->setAmbientColor(ofColor(255, 255, 255));
	//light->setDiffuseColor(ofColor(255, 255, 255));

	light->enable();
	teinte = (0, 0, 0, 0);

}
void Renderer::update()
{
	
}

void Renderer::draw() {
	ofEnableDepthTest();
	ofEnableLighting();
	ofBackground(128);

	camera->begin();

	ofSetColor(128, 0, 0);
	ofDrawBox(0, 0, 0, 10);

	ofDrawAxis(10);
	ofDrawGrid(64);

	root->draw();

	camera->end();

	ofDisableDepthTest();
	ofDisableLighting();

	ofSetColor(teinte);
	ofImage image;
	image.allocate(ofGetWindowWidth(), ofGetWindowHeight(), OF_IMAGE_COLOR);

	image.draw(0, 0);
}

void Renderer::select(int x, int y) {



	ofFbo fbo;
	ofImage img;

	fbo.allocate(ofGetWindowWidth(), ofGetWindowHeight(), GL_RGBA);

	fbo.begin();
	ofClear(0, 0, 0);
	ofColor color = ofColor(1,1,1);
	root->setSelect(true);

	camera->begin();
	root->draw();
	camera->end();

	fbo.end();
	root->setSelect(false);

	ofPixels pix;
	fbo.readToPixels(pix);
	img.setFromPixels(pix);

	ofColor cible = img.getColor(x, y);

	Object * sel = root->findSelection(cible);
	if (sel != nullptr) {
		selection = sel;
	}
	else {
		selection = root;
	}

}

void Renderer::setCamera(ofCamera* cam) {
	camera = cam;
}

void Renderer::imageExport(string name, string ext) {
	string time = ofGetTimestampString("-%y%m%d-%H%M%S - %i");
	string file = name + time + "." + ext;

	ofImage screenShot;
	screenShot.grabScreen(0, 0, ofGetWindowWidth(), ofGetWindowHeight());
	
	screenShot.save(file);
	ofLog() << "screenshot : " << file;
}


void Renderer::setTeinte(ofColor c) {
	teinte = c;
}

Renderer::~Renderer()
{
	if(root != nullptr)
	{
		delete root;
	}
	if (light != nullptr)
	{
		delete light;
	}
}