#pragma once
#include "RayCast.h"

class AbstractObject
{
public:
	AbstractObject();
	virtual ~AbstractObject();
	virtual bool IsIntersectRelevant(Intersection& intersection) = 0;
	virtual bool doesObjectIntersect(const RayCast& ray) = 0;
};

