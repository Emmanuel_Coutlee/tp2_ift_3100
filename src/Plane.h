﻿#pragma once
#include "Object.h"
#include "of3dPrimitives.h"

class Plane : public Object
{
public:
	Plane();
	Plane(float px, float py, float pz, float h, float l);
	~Plane();

	void draw() override;
	void setPositionCall(ofVec3f) override;
	void setRotationCall(ofVec3f) override;
	void setScaleCall(ofVec3f) override;

	
	const ofVec3f getPositionCall() const override;
	const ofVec3f getRotationCall() const override;
	const ofVec3f getScaleCall() const override;

	const ofVec3f getmilieux() const override;

	const ofVec3f getRelativePositionCall() const override;
	const ofVec3f getRelativeRotationCall() const override;
	const ofVec3f getRelativeScaleCall() const override;
	
	const void setNormal(ofPoint& point);
	const ofVec3f getNormal();
	
	bool IsIntersectRelevant(Intersection& intersection);
	bool doesObjectIntersect(const RayCast& ray);
protected:
	ofVec3f normal;

private:
	ofPlanePrimitive* plane;
	
	//ofColor color;
};
