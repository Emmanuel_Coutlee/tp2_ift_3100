
#include "Lumiere.h"


Lumiere::Lumiere() {
	lightMode = ambiant;
	light = new ofLight();
	light->setPosition(0, 0, 0);
	light = new ofLight();

	objColor = ofColor(255, 255, 255);
	light->setAmbientColor(objColor);
	light->enable();


	objName = "light";
	type = "Lumiere";
}

Lumiere::Lumiere(float px, float py, float pz, float rx, float ry, float rz) {
	//TODO
}
Lumiere::~Lumiere() {
	delete light;
}


void Lumiere::draw() {
	ofSetColor(objColor);
	light->setDiffuseColor(objColor);
	light->draw();

	if (childList.size() > 0)
	{
		for (std::list<Object*>::const_iterator it = childList.begin(); it != childList.end(); ++it)
		{
			(*it)->draw();
		}
	}
}

void Lumiere::updateTransform() {
	setPositionCall(getPositionCall());
	setRotationCall(getRotationCall());
	setScaleCall(getScaleCall());

	for (std::list<Object*>::iterator it = childList.begin(); it != childList.end(); ++it)
	{
		(*it)->updateTransform();
	}
}

void Lumiere::setPositionCall(ofVec3f newPos) {

	setGlobalLumiere(newPos);
	ofVec3f parentPos = parent->getPositionCall();
	light->setPosition(newPos + parentPos);
}
void Lumiere::setRotationCall(ofVec3f newRot) {

	ofVec3f parentRot = parent->getRotationCall();
	light->setOrientation(newRot + parentRot);
}

void Lumiere::setScaleCall(ofVec3f newScale) {
	ofVec3f parentScale = parent->getScaleCall();
	light->setScale(newScale * parentScale);
}


void Lumiere::setToRelativeTransform() {
	light->setPosition(getRelativeRotationCall());
	light->setOrientation(getRelativeRotationCall());
	light->setScale(getRelativeScaleCall());

	for (std::list<Object*>::iterator it = childList.begin(); it != childList.end(); ++it)
	{
		(*it)->setToRelativeTransform();
	}
}


const ofVec3f Lumiere::getPositionCall() const {
	return light->getPosition();
}

const ofVec3f Lumiere::getRotationCall() const {
	return light->getOrientationEuler();
}

const ofVec3f Lumiere::getScaleCall() const {
	return light->getScale();
}


const ofVec3f Lumiere::getmilieux() const {
	return getPositionCall();
}

const ofVec3f Lumiere::getRelativePositionCall() const {
	return light->getPosition() - parent->getPositionCall();
}

const ofVec3f Lumiere::getRelativeRotationCall() const {
	return light->getOrientationEuler() - parent->getRotationCall();
}

const ofVec3f Lumiere::getRelativeScaleCall() const {
	return light->getScale() / parent->getScaleCall();
}


void Lumiere::setToAmbiant() {
	ofVec3f pos = light->getPosition();
	ofVec3f rot = light->getOrientationEuler();
	lightMode = ambiant;
	delete light;
	light = new ofLight();
	light->setAmbientColor(objColor);
	light->enable();
	light->setPosition(pos);
	light->setOrientation(rot);
}
void Lumiere::setToDirectionnal() {
	ofVec3f pos = light->getPosition();
	ofVec3f rot = light->getOrientationEuler();
	lightMode = directionnal;
	delete light;
	light = new ofLight();
	light->setDirectional();
	light->enable();
	light->setPosition(pos);
	light->setOrientation(rot);
}
void Lumiere::setToPonctual() {
	ofVec3f pos = light->getPosition();
	ofVec3f rot = light->getOrientationEuler();
	lightMode = pointLight;
	delete light;
	light = new ofLight();
	light->setPointLight();
	light->enable();
	light->setPosition(pos);
	light->setOrientation(rot);
}
void Lumiere::setToProjecteur() {
	ofVec3f pos = light->getPosition();
	ofVec3f rot = light->getOrientationEuler();
	lightMode = spotlight;
	delete light;
	light = new ofLight();
	light->setSpotlight();
	light->enable();
	light->setPosition(pos);
	light->setOrientation(rot);
}


void Lumiere::updateAttenuation(float attenuation) {
	light->setAttenuation(attenuation);
	//light->setSpotlightCutOff(attenuation);
}