#pragma once
#pragma once
#include "of3dPrimitives.h"
#include "Object.h"

class Lumiere : public Object
{
public:
	Lumiere();
	Lumiere(float px, float py, float pz, float rx, float ry, float rz);
	~Lumiere();


	void draw() override;

	void updateTransform() override;

	void setPositionCall(ofVec3f) override;
	void setRotationCall(ofVec3f) override;
	void setScaleCall(ofVec3f) override;

	void setToRelativeTransform() override;

	const ofVec3f getPositionCall() const override;
	const ofVec3f getRotationCall() const override;
	const ofVec3f getScaleCall() const override;

	const ofVec3f getmilieux() const override;

	const ofVec3f getRelativePositionCall() const override;
	const ofVec3f getRelativeRotationCall() const override;
	const ofVec3f getRelativeScaleCall() const override;



	void setToAmbiant() override;
	void setToDirectionnal() override;
	void setToPonctual() override;
	void setToProjecteur() override;


	void updateAttenuation(float attenuation) override;

private:
	ofLight* light;
};
