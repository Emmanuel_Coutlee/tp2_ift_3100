// IFT3100H17_Camera/main.cpp
// Exemple d'une sc�ne avec diff�rentes cam�ras interactives. 

#include "ofMain.h"
#include "InfoApplication.h"
#include "ofxGui.h"
#include "Gui3D.h"


int main()
{
	ofGLWindowSettings windowSettings;

	windowSettings.width = 1280;
	windowSettings.height = 720;

	windowSettings.setGLVersion(2, 1);

	ofCreateWindow(windowSettings);

	ofRunApp(new InfoApplication());
}

