#pragma once

#include "ofMain.h"
#include "Object.h"
#include "RayCast.h"
#include "ObjectSet.h"

class Camera
{
public:

	ofCamera* cam;

	int lastScreenX;
	int lastScreenY;

	float cameraFov;
	float cameraNear;
	float cameraFar;

	ofVec3f cameraPosition;
	ofVec3f cameraTarget;

	ofVec3f cameraForward;
	Camera();
	void setClip(float farclip, float nearclip);
	void setFov(float fov);

	ofCamera* get();
	void translation(int x, int y, int z);
	void tourner(int x, int y);
	void setlastScreenPos(int x, int y);
	void avancer(float scroll);

	void setPerspective(bool perspective);
	bool isPerspective();
	void setCameraTarget(ofVec3f v);

	void rayTrace(ObjectSet* scene);

	virtual RayCast castRay(ofVec2f point) const;
	~Camera();
};
