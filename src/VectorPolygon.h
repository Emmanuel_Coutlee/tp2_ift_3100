#pragma once
#include "ofParameter.h"
#include "ofFbo.h"
#define MAX_POINTS 2000

class VectorPolygon
{
public:
	void setup(int x, int y);
	void update();
	void draw(ofFbo* f);
	void toPath();
	ofParameterGroup polygonParams;
	ofParameter<int> numberOfPolygons;

	ofParameter<int> posX;
	ofParameter<int> posY;

	ofParameter<int> scale;

	ofParameter<float> rotateZ;

	ofParameter<int> red;
	ofParameter<int> green;
	ofParameter<int> blue;
	ofParameter<int> alpha;

	ofParameter<bool> isEditable;
	ofPoint points[MAX_POINTS];
	float rotationZ = 0;
	int limit;
	int thickness;

	void SetPoints(ofPoint p[]);
	VectorPolygon(ofPoint points[], int limit, int thick);
	VectorPolygon(int x, int y);
	VectorPolygon(int x, int y, int numPoly, int scale);
	~VectorPolygon();
};

