#include "VectorGraphics.h"
#include "ofAppNoWindow.h"

void VectorGraphics::setup()
{
	generalVectorModifiers.add(hardness.set("Hardness", 10, 1,100));
}

string VectorGraphics::EnumToString(int shape)
{
	string a;

	switch (shape)
	{
	case Triangle: a = "Triangle";  return a; break;
	case Rectangle:  a = "Rectangle"; return a; break;
	case Pentagon:  a = "Pentagon"; return a; break;
	case Hexagone:  a = "Hexagone"; return a; break;
	case Line:a = "Line"; return a; break;
	case Point:a = "Points"; return a; break;
	default:  a = "[Unknown Mode_type]";    return a; break;
	}
}


void VectorGraphics::update()
{
	
	if(listHasValue)
	{
		for (list<VectorPolygon*>::iterator ti = drawnShapes.begin(); ti != drawnShapes.end(); ti++)
		{
			VectorPolygon* temp = *ti;
			temp->update();

		}
	}
	
	scale = hardness;
}

void VectorGraphics::draw()
{
	if (listHasValue)
	{
		for (list<VectorPolygon*>::iterator ti = drawnShapes.begin(); ti != drawnShapes.end(); ti++)
		{
			
			VectorPolygon* temp = *ti;
			temp->draw(fbo);
			
		}
	}
	fbo->draw(0, 0);
}

void VectorGraphics::updatePolygonArray()
{

	polygons.clear();
	int i = 0;
	for (list<VectorPolygon*>::iterator ti = drawnShapes.begin(); ti != drawnShapes.end(); ti++)
	{

		if (drawnShapes.size() != polygons.size())
		{
			polygons.push_back(*ti);
		}
		else
		{
			polygons[i] = *ti;
		}

	}
}

void VectorGraphics::AddShape(int x, int y,int scale, int numPoly)
{
	listHasValue = true;
	if(numPoly == 0)
	{
		drawnShapes.emplace_back(new VectorPolygon(x, y));
	}
	else
	{
		drawnShapes.emplace_back(new VectorPolygon(x, y, scale,numPoly));
	}
	
}



void VectorGraphics::drawLine(ofPoint points[], int limit)
{
	ofColor objColor(255, 0, 0);
	fbo->begin();
	ofPushStyle();
	ofClear(255, 0, 0);
	ofSetColor(objColor);
	ofFill();
	
	ofPopStyle();
	fbo->end();
}

void VectorGraphics::drawPoint(ofPoint points[], int limit, int radius)
{
	
	for (int i = 0; i < limit - 1; i++)
	{
		drawnShapes.emplace_back(new VectorPolygon(points[i].x,points[i].y,radius, 60));
	}
	
}


VectorGraphics::VectorGraphics()
{
	
	setup();
	listHasValue = true;
	drawnShapes.emplace_back(new VectorPolygon(0,0));
}
VectorGraphics::VectorGraphics(int px, int py,Shape shape)
{
	
	setup();
	AddShape(px, py,hardness, shape);
	//guiVectorModifier.setup(polygons[])
	
	
}

VectorGraphics::~VectorGraphics()
{
	/*if (childList.size()> 0)
	{
		for (std::list<Object*>::iterator it = childList.begin(); it != childList.end(); ++it)
		{
			delete *it;
		}
	}

	parent = nullptr;
	delete fbo;*/
	/*for (list<VectorPolygon*>::iterator ti = drawnShapes.end(); ti != drawnShapes.begin(); ti--)
	{
		delete &ti;

	}*/
}
/*void VectorGraphics::DeleteShapes()
{
	for (list<ofFbo>::iterator ti = vectDrawer->vectorShapes.begin(); ti != vectDrawer->vectorShapes.end(); ti++)
	{
		ti->clear();

	}
	vectDrawer->frameBuffers.clear();
	vectDrawer->vectorShapes.clear();
	deleteShapes = false;
}*/