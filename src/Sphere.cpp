﻿#include "Sphere.h"
#include "ofCamera.h"

Sphere::Sphere()
{
	sphere = new ofSpherePrimitive(100,100);
	sphere->setRadius(100);
	sphere->setPosition(0, 0, 0);
	sphere->setScale(1, 1, 1);

	objName = "shpere";

	objColor.set(0, 0, 0);
}

Sphere::Sphere(float px, float py, float pz, float r)
{
	sphere = new ofSpherePrimitive();
	sphere->setPosition(px, py, pz);
	sphere->setRadius(r);
}


Sphere::~Sphere()
{

	delete sphere;
}

void Sphere::draw()
{
	ofSetColor(objColor);
	sphere->draw();

	for (std::list<Object*>::const_iterator it = childList.begin(); it != childList.end(); ++it)
	{
		(*it)->draw();
	}
}

void Sphere::setPositionCall(ofVec3f newPos)
{
	ofVec3f parentPos = parent->getPositionCall();
	sphere->setPosition(newPos +parentPos);
}

void Sphere::setRotationCall(ofVec3f newRot)
{
	ofVec3f parentRot = parent->getRotationCall();
	sphere->setOrientation(newRot + parentRot);
}

void Sphere::setScaleCall(ofVec3f newScale)
{
	ofVec3f parentScale = parent->getScaleCall();
	sphere->setScale(newScale * parentScale);
}

const ofVec3f Sphere::getPositionCall() const
{
	return sphere->getPosition();
}

const ofVec3f Sphere::getRotationCall() const
{
	return sphere->getOrientationEuler();
}

const ofVec3f Sphere::getScaleCall() const
{
	return sphere->getScale();
}

const ofVec3f Sphere::getRelativePositionCall() const
{
	return sphere->getPosition() - parent->getPositionCall();
}

const ofVec3f Sphere::getRelativeRotationCall() const
{
	return sphere->getOrientationEuler() - parent->getRotationCall();
}

const ofVec3f Sphere::getRelativeScaleCall() const
{
	return sphere->getScale() / parent->getScaleCall();
}

const ofVec3f Sphere::getmilieux() const {
	return getPositionCall();
}
bool Sphere::IsIntersectRelevant(Intersection& intersection)
{
	// Transform ray so we can consider origin-centred sphere
	RayCast localRay = intersection.ray;
	localRay.origin -= this->getPositionCall();

	// Calculate quadratic coefficients
	float a = localRay.direction.squareDistance(localRay.direction);
	float b = 2 * localRay.direction.dot(localRay.origin);
	float c = localRay.origin.squareDistance(localRay.origin) - pow(sphere->getRadius(), 2);

	// Check whether we IsRayIntersectRelevant
	float discriminant = pow(b, 2) - 4 * a * c;

	if (discriminant < 0.0f)
	{
		return false;
	}

	
	float point1 = (-b - std::sqrt(discriminant)) / (2 * a);
	float point2 = (-b + std::sqrt(discriminant)) / (2 * a);

	// First check if close intersection is valid
	if (point1 > RAY_T_MIN && point1 < intersection.t)
	{
		intersection.t = point1;
	}
	else if (point2 > RAY_T_MIN && point2 < intersection.t)
	{
		intersection.t = point2;
	}
	else
	{
		
		return false;
	}

	
	//intersection.pObject = this->getParentObj();
	intersection.color = this->getColorCall();

	return true;

}

bool Sphere::doesObjectIntersect(const RayCast& ray)
{
	RayCast localRay = ray;
	localRay.origin -= this->getPositionCall();

	// Trouver  les coefficients quadratic 
	float a = localRay.direction.squareDistance(localRay.direction);
	float b = 2 * localRay.direction.dot(localRay.origin);
	float c = localRay.origin.squareDistance(localRay.origin) - pow(sphere->getRadius(), 2);

	// Regarde si IsRayIntersectRelevant
	float discriminant = pow(b, 2) - 4 * a * c;

	if (discriminant < 0.0f)
	{
		return false;
	}

	// Trouver deux point d'intersection, pointIntersection1 proche and pointIntersection2 loin
	float pointIntersection1 = (-b - std::sqrt(discriminant)) / (2 * a);
	if (pointIntersection1 > RAY_T_MIN && pointIntersection1 < ray.tMax)
		return true;

	float pointIntersection2 = (-b + std::sqrt(discriminant)) / (2 * a);
	if (pointIntersection2 > RAY_T_MIN && pointIntersection2 < ray.tMax)
		return true;

	return false;
}
