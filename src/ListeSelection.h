#pragma once
#pragma once
#include <list>
#include <string>
#include "ofMain.h"
#include "Object.h"

class ListeSelection : public Object
{
public:


	ListeSelection();
	~ListeSelection();


	void draw() override;

	void updateTransform() override;

	void setPositionCall(ofVec3f) override;
	void setRotationCall(ofVec3f) override;
	void setScaleCall(ofVec3f) override;

	const ofVec3f getPositionCall() const override;
	const ofVec3f getRotationCall() const override;
	const ofVec3f getScaleCall() const override;

	const ofVec3f getmilieux() const override;

	const ofVec3f getRelativePositionCall() const override;
	const ofVec3f getRelativeRotationCall() const override;
	const ofVec3f getRelativeScaleCall() const override;


	void setColorCall(ofColor) override;

	void addObj(Object * o);
	void clear();


private:
	ofVec3f pos;
	ofVec3f rot;
	ofVec3f scale;
	list<Object *> listObjet;

};