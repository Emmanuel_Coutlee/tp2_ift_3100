#include "ObjectSet.h"

ObjectSet::ObjectSet()
{
}

ObjectSet::~ObjectSet()
{
}

void ObjectSet::addShape(Object* shape)
{
	shapes.push_back(shape);
}

bool ObjectSet::IsIntersectRelevant(Intersection& intersection)
{
	bool doesIntersect = false;

	for (std::vector<Object*>::iterator iter = shapes.begin();
		iter != shapes.end();
		++iter)
	{
		Object *curShape = *iter;
		/*if (curShape->IsIntersectRelevant(intersection))
		{
			doesIntersect = true;
		}*/
			
	}

	return doesIntersect;
}

bool ObjectSet::doesObjectIntersect(const RayCast& ray)
{
	for (std::vector<Object*>::iterator iter = shapes.begin();
		iter != shapes.end();
		++iter)
	{
		Object *curShape = *iter;
		/*if (curShape->doesObjectIntersect(ray))
		{
			return true;
		}*/
			
	}

	return false;
}

void ObjectSet::setColorCall(ofColor color, ofPoint pointIntersect)
{
	for (std::vector<Object*>::iterator iter = shapes.begin();
		iter != shapes.end();
		++iter)
	{
		Object *curShape = *iter;
		vector<ofIndexType>* meshPositions = &curShape->getMesh().getIndices();
		for(std::vector<ofIndexType>::iterator iteratr = meshPositions->begin();iteratr != meshPositions->end(); ++iteratr)
		{
			if(pointIntersect == curShape->getMesh().getVertex(*iteratr))
			{
				curShape->getMesh().setColor(*iteratr, color);
			}
		}
	}
}


