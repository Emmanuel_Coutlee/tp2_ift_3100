#pragma once

#include "ofMain.h"
#include "renderer.h"
#include "camera.h"
#include "VectorGraphics.h"
#include "Gui3D.h"
#include "GlobalGui.h"
#include "ListeSelection.h"
#define MAX_POINTS 2000

class InfoApplication : public ofBaseApp
{
public:

	
	Camera * camera;
	Renderer * renderer;
	Gui3D * gui3D;
	ListeSelection * listSelect;

	GlobalGui * gGui;


	VectorGraphics * vectGraphics;
	bool mouseIsDragged;
	bool ctrlKeyIsDown;

	GlobalGui::Mode currentmode = GlobalGui::vectors;
	int limit = 0;
	ofPoint points[MAX_POINTS];
	VectorGraphics::Shape drawingMode = VectorGraphics::Triangle;
	ofImage cursorDrawImage;
	bool drawPaintBrush = false;
	ofImage cursorDeleteImage;
	bool drawEraser = false;
	ofImage cursorColorImage;
	bool drawSetColor = false;
	ObjectSet* sceneObjectsRay;
	bool IMAFIRINMAHLAZOR = false;


	InfoApplication();

	void setup();

	void update();

	void draw();
	void keyReleased(int key);
	void keyPressed(int key);
	void mouseDragged(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void mouseScrolled(int x, int y, float scrollX, float scrollY);
	void mousePressed(int x, int y, int button);
	void exit();




	~InfoApplication();
};
