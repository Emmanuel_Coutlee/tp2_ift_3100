﻿#pragma once
#include "of3dPrimitives.h"
#include "Object.h"

class Sphere : public Object
{
public:
	Sphere();
	Sphere(float px, float py, float pz, float r);
	~Sphere();


	void draw() override;
	void setPositionCall(ofVec3f) override;
	void setRotationCall(ofVec3f) override;
	void setScaleCall(ofVec3f) override;

	const ofVec3f getPositionCall() const override;
	const ofVec3f getRotationCall() const override;
	const ofVec3f getScaleCall() const override;

	const ofVec3f getmilieux() const override;

	const ofVec3f getRelativePositionCall() const override;
	const ofVec3f getRelativeRotationCall() const override;
	const ofVec3f getRelativeScaleCall() const override;

	bool IsIntersectRelevant(Intersection& intersection);
	bool doesObjectIntersect(const RayCast& ray);
private:
	ofSpherePrimitive* sphere;
	//ofColor color;
};
