#pragma once

#include "ofMain.h"
#include "Object.h"


class Renderer
{
public:

	Renderer();

	ofCamera * camera;
	ofVec3f cameraPosition;
	ofVec3f cameraTarget;
	Object* root;
	Object* selection;

	ofLight* light;

	ofColor teinte;

	void setup();
	void update();
	void draw();
	void setCamera(ofCamera* cam);
	void setTeinte(ofColor c);

	void select(int x, int y);



	void imageExport(string name, string ext);

	~Renderer();
};
