
#pragma once
#include "RayCast.h"
#include "AbstractObject.h"


class ObjectSet : public AbstractObject
{
protected:
	std::vector<Object*> shapes;

public:
	ObjectSet();

	virtual ~ObjectSet();

	void addShape(Object* shape);

	bool IsIntersectRelevant(Intersection& intersection) override;
	bool doesObjectIntersect(const RayCast& ray) override;
	void setColorCall(ofColor color,ofPoint pointIntersect);
};
