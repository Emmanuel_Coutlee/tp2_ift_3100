#ifndef _TEST_APP
#define _TEST_APP


#include "ofMain.h"
#include "ofxShader.h"
#include "ofxFBOTexture.h"

class testApp : public ofBaseApp{
public:

	void setup();
	void update();
	void draw();

	ofImage colormap,bumpmap;
	ofxShader shader;
	GLUquadricObj *quadratic;
	
};

#endif

