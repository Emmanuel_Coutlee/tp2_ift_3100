#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
	ofBackground(0,0,0);
	ofDisableArbTex();
	ofEnableAlphaBlending();
	ofSetFrameRate(60);
	glEnable(GL_DEPTH_TEST);
	
	shader.setup("shaders/displace");
	
	colormap.loadImage("earth.jpg");
	bumpmap.loadImage("earth-bumps.jpg");

	quadratic = gluNewQuadric();
	gluQuadricTexture(quadratic, GL_TRUE);
	gluQuadricNormals(quadratic, GLU_SMOOTH);
}

//--------------------------------------------------------------
void testApp::update(){
	ofSetWindowTitle(ofToString(ofGetFrameRate()));
}

//--------------------------------------------------------------
void testApp::draw(){
	shader.begin();
	shader.setTexture("colormap", colormap, 1); 
	shader.setTexture("bumpmap", bumpmap, 2);
	shader.setUniform("maxHeight",25);
	
	ofTranslate(ofGetWidth()/2, ofGetHeight()/2);
	ofRotateY(360*sinf(float(ofGetFrameNum())/500.0f));
	ofRotate(-90,1,0,0);
	gluSphere(quadratic, 150, 400, 400);
	
	shader.end();
}

